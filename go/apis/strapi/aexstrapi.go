package aexstrapi

import (
	"net/http"
	"bytes"
	"encoding/json"
	"log"

	"github.com/labstack/echo"
)

func New(e *echo.Echo) {
	raextrapi := e.Group("/aexspi")
	raextrapi.Match([]string{"POST"}, "/login", login)
}

type Login struct {
	Identifier 	string 			`json:"identifier"`
	Password   	string 			`json:"password"`
}

type StrapiLogged struct {
	Jwt  		string     		`json:"jwt"`
	User 		StrapiUser 		`json:"user"`
}

type StrapiUser struct {
	Username  	string         	`json:"username"`
	Id        	int            	`json:"id"`
	Email     	string         	`json:"email"`
	Provider  	string         	`json:"provider"`
	Confirmed 	bool           	`json:"confirmed"`
	Blocked   	bool           	`json:"blocked"`
	Role      	StrapiUserRole 	`json:"role"`
}

type StrapiUserRole struct {
	Id          int   			`json:"id"`
	Name        string			`json:"name"`
	Description string			`json:"description"`
	Type        string			`json:"type"`
}

type RespUserLoggedIn struct {
	Message		string			`json:"message"`
	Code		int				`json:"code"`
	Jwt			string			`json:"jwt"`
	Username	string			`json:"username"`
	Type		string			`json:"type"`
}

type RespError struct {
	Message		string			`json:"message"`
	Code		int				`json:"code"`
}

func login(c echo.Context) error {
	json_login := new(Login)
	if err := c.Bind(json_login); err != nil {
		return err
	}

	if json_login.Identifier != "" && json_login.Password != "" {

		buf := new(bytes.Buffer)
		json.NewEncoder(buf).Encode(json_login)

		req, _ := http.NewRequest("POST", "http://localhost:1337/auth/local", buf)
		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		res, e := client.Do(req)

		if e != nil {
			log.Print(e)
		}

		decoder := json.NewDecoder(res.Body)
		var strapi_loged_user StrapiLogged
		e = decoder.Decode(&strapi_loged_user)

		if e != nil {
			panic(e)
		}

		if strapi_loged_user.Jwt != "" {
			resp_user_loggedin := RespUserLoggedIn{
				Message:	"logged-in",
				Code:		200,
				Jwt:		strapi_loged_user.Jwt,
				Username:	strapi_loged_user.User.Username,
				Type:		strapi_loged_user.User.Role.Type,
			}
			return c.JSON(http.StatusCreated, resp_user_loggedin)

		} else {
			resp_error := RespError{
				Message:	"Error credentials please review your identifier and password",
				Code:		401,
			}
			return c.JSON(http.StatusCreated, resp_error)
		}

	} else {
		resp_error := RespError{
			Message:		"Missing JSON parameters, needed (identifier, password)",
			Code:			400,
		}
		return c.JSON(http.StatusCreated, resp_error)
	}
}
package aexspidiv

import (
	"net/http"
	"fmt"
    "github.com/go-pg/pg/v9"
    "github.com/go-pg/pg/v9/orm"
    "github.com/labstack/echo"
)

func Div(e *echo.Echo) {
    rtrASD := e.Group("div")
	rtrASD.Match([]string{"GET"}, "get", get)
	rtrASD.Match([]string{"POST"},"cre", cre)
	rtrASD.Match([]string{"PUT"}, "upd", upd)
}

type AendlyxCryptoRealTimeValue struct {
    Usd float64 `json:"usd"`
    Eur float64 `json:"eur"`
    Pn int `json:"pn"`
    Pin int `json:"pin"`
    Wayperc float64 `json:"wayperc"`
}

func handler(c echo.Context) error {
    aendlyxcryptorealtimevalue := new(AendlyxCryptoRealTimeValue)
    if err := c.Bind(aendlyxcryptorealtimevalue); err != nil {
		return err
    }
    return c.JSON(http.StatusCreated, aendlyxcryptorealtimevalue)
}

func get(e *echo.Echo){
	if err := c.Bind() ; err != nil {
		return err
	}
}
func cre(e *echo.Echo){
	if err := c.Bind() ; err != nil {
		return err
	}
}
func upd(e *echo.Echo){
	if err := c.Bind() ; err != nil {
		return err
	}
}
import Vue from 'vue'
import Router from 'vue-router'
import RootView from './pages/root/view.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'RootView',
      component: RootView
    }
  ]
})
